<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class AddProductFormType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);

		$builder
			->add('name',TextType::class, [
				'label' => 'Product name',
			])
			->add('price', NumberType::class)
			->add('Add', SubmitType::class, [
				'label' => 'Add Product',
				'attr' => [
					'class' => 'btn btn-lg btn-success btn-block',
				]
			]);
	}
}