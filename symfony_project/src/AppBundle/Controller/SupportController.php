<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Products;
use AppBundle\Form\Type\AddProductFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SupportController extends Controller
{
//    /**
//     * @Route("/", name="homepage")
//     */
//    public function indexAction()
//    {
//    	$form = $this->createForm(AddProductFormType::class, null, [
//    		'action' => $this->generateUrl('handle_form_submission')
//	    ]);
//
//        // replace this example code with whatever you need
//        return $this->render('support/index.html.twig', [
//            'our_form' => $form->createView(),
//        ]);
//    }

	/**
	 * @param Request $request
	 * @Route("/form-submission", name="handle_form_submission")
	 * @Method("POST"))
	 */
	public function handleFormSubmissionAction(Request $request)
	{
		$form = $this->createForm(AddProductFormType::class);

		$form->handleRequest($request);
//
		if(!$form->isSubmitted() ||  !$form->isValid()){
			return $this->redirectToRoute('list_product');
		}
//
		$data = $form->getData();
		dump($data);
		return $this->redirectToRoute('list_product');
    }

	/**
	 * @Route("/", name="list_product")
	 */
	public function showProduct()
	{
		$product = $this->getDoctrine()
			->getRepository(Products::class)
			->findAll();

		return $this->render('product/listProduct.html.twig', [
			'list_product' => $product,
		]);
    }

	/**
	 * @param $id
	 * @Route("/detail/{id}", name="detail_product")
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function showDetailProduct($id)
	{
		$detail = $this->getDoctrine()
			->getRepository(Products::class)
			->find($id);
		return $this->render('product/detailProduct.html.twig', [
			'detail_product' => $detail,
		]);
    }
}
