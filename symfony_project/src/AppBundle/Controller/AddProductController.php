<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Products;
use AppBundle\Form\Type\ProductsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddProductController extends Controller
{
	/**
	 * @Route("/addproduct", name="add_product")
	 * @return Response
	 */
	public function AddProductAction(Request $request)
	{
		$product = new Products();

		$form = $this->createForm(ProductsType::class, $product, [

		]);

		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid()){

			$em = $this->getDoctrine()->getManager();
			$em->persist($product);
			$em->flush();

			$this->addFlash('success', 'Add product success !!!');
			dump($product->getId());

//			return $this->redirectToRoute('list_product');
		}

		return $this->render('product/addProduct.html.twig', [
			'add_product_form' => $form->createView(),
		]);
	}
}